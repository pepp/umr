static struct umr_bitfield mmCLK3_0_CLK3_CLK_PLL_REQ[] = {
	 { "FbMult_int", 0, 8, &umr_bitfield_default },
	 { "PllSpineDiv", 12, 15, &umr_bitfield_default },
	 { "FbMult_frac", 16, 31, &umr_bitfield_default },
};
static struct umr_bitfield mmCLK3_0_CLK3_CLK2_DFS_CNTL[] = {
	 { "CLK2_DIVIDER", 0, 6, &umr_bitfield_default },
};
