[Unit]
Description="Dump amdgpu waves when a hang is detected"

[Service]
ExecStart=@CMAKE_INSTALL_PREFIX@/@CMAKE_INSTALL_BINDIR@/umr --instance %i --autodump
Restart=always
RestartSec=10

[Install]
WantedBy=multi-user.target